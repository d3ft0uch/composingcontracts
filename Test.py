import datetime

from BinominalLattice import *
from helper import apply_helper


def today():
    return datetime.date.today()


def test_1():
    lattice = ConstantBL(5)
    assert (apply_helper(lattice._apply(0), 0) == 5)
    assert (apply_helper(lattice._apply(1), 0) == 5)
    assert (apply_helper(lattice._apply(1), 1) == 5)
    assert (apply_helper(lattice._apply(10), 0) == 5)
    assert (apply_helper(lattice._apply(10), 9) == 5)


def test_2():
    lattice = PassThroughBL(lambda date: lambda idx: date)
    assert (apply_helper(lattice._apply_date(today()), 0) == today())
    assert (apply_helper(lattice._apply_date(today()), 1) == today())
    assert (apply_helper(lattice._apply_date(today()), 2) == today())


def test_3():
    lattice = GenerateBL(5, 100, 1.1)
    assert (apply_helper(lattice._apply(0), 0) == 100)
    assert (abs(apply_helper(lattice._apply(4), 0) - 68.3) < 0.01)
    assert (abs(apply_helper(lattice._apply(4), 2) - 100) < 0.01)
    assert (abs(apply_helper(lattice._apply(4), 4) - 146.41) < 0.01)
    assert (lattice.size() == 5)


def test_4():
    lattice = GenerateBL(0, 100, 1.1)
    assert (apply_helper(lattice._apply(0), 0) == 100)
    assert (lattice.size() == 0)


def test_5():
    lattice = GenerateBL(5, 100, 1.1).map(lambda x: x * 2)
    assert (apply_helper(lattice._apply(0), 0) == 100 * 2)
    assert (abs(apply_helper(lattice._apply(4), 0) - 68.3 * 2) < 0.01)
    assert (abs(apply_helper(lattice._apply(4), 2) - 100 * 2) < 0.01)
    assert (abs(apply_helper(lattice._apply(4), 4) - 146.41 * 2) < 0.01)


def test_6():
    source = GenerateBL(5, 100, 1.1)
    lattice = PropagateLeftBL(source, lambda tup: (tup[0] + tup[1]) / 2.0)
    assert (abs(apply_helper(lattice._apply(3), 0) - 75.47) < 0.01)
    assert (abs(apply_helper(lattice._apply(3), 2) - 110.5) < 0.01)
    assert (abs(apply_helper(lattice._apply(3), 3) - 133.70) < 0.01)
    assert (lattice.size() == 4)
    assert (source.size() == lattice.size() + 1)


if __name__ == '__main__':
    test_1()
    test_2()
    test_3()
    test_4()
    test_5()
    test_6()
    print "OK"
