from datetime import date, timedelta

from helper import apply_helper


class BinomialLattice(object):
    def __init__(self):
        pass

    def _apply(self, arg):
        raise NotImplementedError

    def _apply_date(self, _date):
        return self._apply(abs((date.today() - _date).days))

    @staticmethod
    def probability():
        return 0.5

    def start_val(self):
        left = self._apply(0)
        return apply_helper(left, 0)

    def zip(self, lattice):
        obj = BinomialLattice()
        obj._apply = lambda i: lambda j: (apply_helper(self._apply(i), j), apply_helper(lattice._apply(i), j))
        return obj

    def map(self, func):
        obj = BinomialLattice()
        obj._apply = lambda i: lambda j: func(apply_helper(self._apply(i), j))
        return obj

    def lift(self, func, lattice):
        obj = BinomialLattice()
        obj._apply = lambda i: lambda j: func((apply_helper(self._apply(i), j), apply_helper(lattice._apply(i), j)))
        return obj

    def __str__(self):
        string = ''
        for i in xrange(0, 4):
            for j in xrange(0, i + 1):
                value = apply_helper(self._apply(i), j)
                string += str(round(value, 4))
                string += '\t'
            string += '\n'
        string += '...'
        return string


class BinomialLatticeBounded(BinomialLattice):
    def _apply(self, arg):
        raise NotImplementedError

    def __init__(self):
        super(BinomialLatticeBounded, self).__init__()

    def size(self):
        raise NotImplementedError

    def zip(self, lattice):
        obj = BinomialLatticeBounded()
        obj._apply = lambda i: lambda j: (apply_helper(self._apply(i), j), apply_helper(lattice._apply(i), j))
        obj.size = self.size
        return obj

    def map(self, func):
        obj = BinomialLatticeBounded()
        obj._apply = lambda i: lambda j: func(apply_helper(self._apply(i), j))
        obj.size = self.size
        return obj

    def lift(self, func, lattice):
        obj = BinomialLatticeBounded()
        obj._apply = lambda i: lambda j: func((apply_helper(self._apply(i), j), apply_helper(lattice._apply(i), j)))
        obj.size = self.size
        return obj

    def __str__(self):
        string = ''
        for i in xrange(0, self.size()):
            for j in xrange(0, i + 1):
                value = apply_helper(self._apply(i), j)
                string += str(round(value, 4))
                string += '\t'
            string += '\n'
        return string


class ConstantBL(BinomialLattice):
    def __init__(self, k):
        super(ConstantBL, self).__init__()
        self.k = k

    def _apply(self, arg):
        return lambda j: self.k


class PassThroughBL(BinomialLattice):
    def __init__(self, func):
        super(PassThroughBL, self).__init__()
        self.func = func

    def _apply(self, arg):
        return self._apply_date(date.today() + timedelta(arg))

    def _apply_date(self, _date):
        return self.func(_date)


class PassThroughBoundedBL(BinomialLatticeBounded):
    def __init__(self, func, size):
        super(PassThroughBoundedBL, self).__init__()
        self.func = func
        self._size = size

    def _apply(self, arg):
        return self._apply_date(date.today() + timedelta(arg))

    def _apply_date(self, _date):
        return self.func(_date)

    def size(self):
        return self._size


class GenerateBL(BinomialLatticeBounded):
    def __init__(self, size, start_val, up_factor, up_probability=0.5):
        super(GenerateBL, self).__init__()
        self._size = size
        self.start_val = start_val
        self.up_factor = up_factor
        self.up_probability = up_probability
        self.cache = []
        self.down_factor = 1.0 / up_factor
        self.cache.append([start_val])
        for i in xrange(1, self.size()):
            arr = []
            first = self.down_factor * self.cache[i - 1][0]
            arr.append(first)
            for j in xrange(1, i + 1):
                val = up_factor * self.cache[i - 1][j - 1]
                arr.append(val)
            self.cache.append(arr)

    def size(self):
        return self._size

    def _apply(self, arg):
        return self.cache[arg]


class PropagateLeftBL(BinomialLatticeBounded):
    def __init__(self, source, func):
        super(PropagateLeftBL, self).__init__()
        self.source = source
        self.func = func
        self.cache = []
        for i in xrange(0, self.size()):
            self.cache.append([None] * (i + 1))
        for i in reversed(xrange(0, self.size())):
            for j in xrange(0, i + 1):
                x = apply_helper(source._apply(i + 1), j)
                y = apply_helper(source._apply(i + 1), j + 1)
                self.cache[i][j] = func((x, y))

    def _apply(self, i):
        if self.size() > 1:
            return self.cache[i]
        else:
            return self.source._apply(i)

    def size(self):
        if self.source.size() > 1:
            return self.source.size() - 1
        else:
            return 1
