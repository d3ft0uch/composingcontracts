def apply_helper(left, right):
    if isinstance(left, list) or isinstance(left, tuple) or isinstance(left, dict):
        return left[right]
    elif callable(left):
        if isinstance(right, tuple) or isinstance(right, list):
            length = len(right)
            if length == 1:
                return left(right[0])
            elif length == 2:
                return left(right[0], right[1])
            else:
                raise Exception("Too many args to apply")
        else:
            return left(right)
    elif hasattr(left, '_apply'):
        return left._apply(right)
    else:
        raise Exception("Cannot find what to do.")
