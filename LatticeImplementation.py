import math
from datetime import date
from BinominalLattice import GenerateBL, PropagateLeftBL, ConstantBL, PassThroughBL, PassThroughBoundedBL
from ComposingContracts import *
from helper import apply_helper


class PROpt:
    __metaclass__ = ABCMeta

    def plus(self, that):
        return Lift2PR(lambda tup: tup[0] + tup[1], self, that)

    def minus(self, that):
        return Lift2PR(lambda tup: tup[0] - tup[1], self, that)

    def times(self, that):
        return Lift2PR(lambda tup: tup[0] * tup[1], self, that)

    def eq(self, that):
        return Lift2PR(lambda tup: tup[0] == tup[1], self, that)

    def neq(self, that):
        return Lift2PR(lambda tup: tup[0] != tup[1], self, that)

    def gt(self, that):
        return Lift2PR(lambda tup: tup[0] > tup[1], self, that)

    def lt(self, that):
        return Lift2PR(lambda tup: tup[0] < tup[1], self, that)

    def gte(self, that):
        return Lift2PR(lambda tup: tup[0] >= tup[1], self, that)

    def lte(self, that):
        return Lift2PR(lambda tup: tup[0] <= tup[1], self, that)


class ConstPR(PROpt):
    def __init__(self, k):
        self.k = k


class DatePR(PROpt):
    def __init__(self):
        pass


class CondPR(PROpt):
    def __init__(self, cond, a, b):
        self.cond = cond
        self.a = a
        self.b = b


class LiftPR(PROpt):
    def __init__(self, lifted, o):
        self.lifted = lifted
        self.o = o


class Lift2PR(PROpt):
    def __init__(self, lifted, o1, o2):
        self.lifted = lifted
        self.o1 = o1
        self.o2 = o2


class LookupPR(PROpt):
    def __init__(self, lookup):
        self.lookup = lookup


class Snell(PROpt):
    def __init__(self, date, c):
        self.date = date
        self.c = c


class Disc(PROpt):
    def __init__(self, date, c):
        self.date = date
        self.c = c


class Absorb(PROpt):
    def __init__(self, date, c):
        self.date = date
        self.c = c


class Exch(PROpt):
    def __init__(self, curr):
        self.curr = curr


class Environment:
    def __init__(self, interest_rate, exchange_rates, lookup):
        self.interest_rate = interest_rate
        self.exchange_rates = exchange_rates
        self.lookup = lookup


def obs_to_PROpt(observable):
    if isinstance(observable, Const):
        return ConstPR(observable.k)
    elif isinstance(observable, Lift):
        return LiftPR(observable.lifted, obs_to_PROpt(observable.o))
    elif isinstance(observable, Lift2):
        return Lift2PR(observable.lifted, obs_to_PROpt(observable.o1), obs_to_PROpt(observable.o2))
    elif isinstance(observable, DateObs):
        return DatePR()
    elif isinstance(observable, Lookup):
        return LookupPR(observable.lookup)
    else:
        raise Exception(observable)


def binomial_valuation(pr, market_data):
    if isinstance(pr, ConstPR):
        return ConstantBL(pr.k)
    elif isinstance(pr, DatePR):
        return PassThroughBL(lambda ___date: lambda idx: ___date)
    elif isinstance(pr, CondPR):
        o = binomial_valuation(pr.cond, market_data)
        ca = binomial_valuation(pr.a, market_data)
        cb = binomial_valuation(pr.b, market_data)
        return PassThroughBL(
            lambda ___date: lambda idx: apply_helper(ca._apply_date(___date), idx) if apply_helper(
                o._apply_date(___date),
                idx)
            else apply_helper(cb._apply_date(___date), idx))
    elif isinstance(pr, LiftPR):
        lifted = pr.lifted
        obs = binomial_valuation(pr.o, market_data)
        return PassThroughBL(lambda ___date: lambda idx: lifted(apply_helper(obs._apply_date(___date), idx)))
    elif isinstance(pr, Lift2PR):
        obs1 = binomial_valuation(pr.o1, market_data)
        obs2 = binomial_valuation(pr.o2, market_data)
        return PassThroughBL(
            lambda ___date: lambda idx: pr.lifted(apply_helper(obs1._apply_date(___date), idx),
                                                  apply_helper(obs2._apply_date(___date), idx)))
    elif isinstance(pr, LookupPR):
        return apply_helper(market_data.lookup, pr.lookup)

    elif isinstance(pr, Exch):
        exchange_rate = apply_helper(market_data.exchange_rates, pr.curr)
        func = lambda __date: lambda idx: apply_helper(exchange_rate._apply_date(__date), idx)
        return PassThroughBL(func)
    elif isinstance(pr, Disc):
        days_until_maturity = abs((date.today() - pr.date).days)
        con = binomial_valuation(pr.c, market_data)
        interest_rates = market_data.interest_rate
        _process = PassThroughBoundedBL(lambda ___date: lambda idx: apply_helper(con._apply_date(___date), idx),
                                        days_until_maturity + 1)
        return discount(_process, interest_rates)

    elif isinstance(pr, Snell):
        days_until_maturity = abs((date.today() - pr.date).days)
        con = binomial_valuation(pr.c, market_data)
        interest_rates = market_data.interest_rate
        _process = PassThroughBoundedBL(lambda ___date: lambda idx: apply_helper(con._apply_date(___date), idx),
                                        days_until_maturity + 1)
        result = _process
        for i in xrange(0, _process.size() + 1):
            discounted = discount(result, interest_rates)
            result = discounted.zip(_process).map(lambda tup: max(tup[0], tup[1]))
        return result
    else:
        raise Exception


def contract_to_PROpt(contract):
    if isinstance(contract, Zero):
        return ConstPR(0)
    elif isinstance(contract, One):
        return Exch(contract.currency)
    elif isinstance(contract, Give):
        return contract_to_PROpt(Scale(Const(-1), contract.contract))
    elif isinstance(contract, Scale):
        return Lift2PR(lambda a, b: a * b, obs_to_PROpt(contract.scale), contract_to_PROpt(contract.contract))
    elif isinstance(contract, And):
        return Lift2PR(lambda a, b: a + b, contract_to_PROpt(contract.contract1),
                       contract_to_PROpt(contract.contract2))
    elif isinstance(contract, Or):
        return Lift2PR(lambda a, b: max(a, b), contract_to_PROpt(contract.contract1),
                       contract_to_PROpt(contract.contract2))
    elif isinstance(contract, Cond):
        return CondPR(obs_to_PROpt(contract.cond), contract_to_PROpt(contract.contract1),
                      contract_to_PROpt(contract.contract2))
    elif isinstance(contract, When):
        return Disc(contract.date, contract_to_PROpt(contract.contract))
    elif isinstance(contract, Anytime):
        return Snell(contract.date, contract_to_PROpt(contract.contract))


def binomial_price_tree(days, start_val, annualized_volatility, probability=0.5):
    business_days_in_year = 365.0
    fraction_of_year = (days * 1.0) / business_days_in_year
    change_factor_up = vol2chfactor(annualized_volatility, fraction_of_year)
    return GenerateBL(days + 1, start_val, change_factor_up)


def vol2chfactor(vol, fractionOfYear):
    return math.exp(vol * math.sqrt(fractionOfYear))


def discount(to_discount, interest_rates):
    averaged = PropagateLeftBL(to_discount, lambda tup: (tup[0] + tup[1]) / 2.0)
    zipped = averaged.zip(interest_rates)
    return zipped.map(lambda tup: tup[0] / (1.0 + tup[1]))
