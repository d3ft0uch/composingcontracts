import datetime
from LatticeImplementation import *


def usd(amount):
    return Scale(Const(amount), One("USD"))


def buy(contract, amount):
    return And(contract, Give(usd(amount)))


def sell(contract, amount):
    return And(Give(contract), usd(amount))


def zcb(maturity, notional, currency):
    return When(maturity, Scale(Const(notional), One(currency)))


def option(contract):
    return Or(contract, Zero())


def european_call_option(at, c1, strike):
    return When(at, option(buy(c1, strike)))


def european_put_option(at, c1, strike):
    return When(at, option(sell(c1, strike)))


def american_call_option(at, c1, strike):
    return Anytime(at, option(buy(c1, strike)))


def american_put_option(at, c1, strike):
    return Anytime(at, option(sell(c1, strike)))


def stock(symbol):
    return Scale(Lookup(symbol), One("USD"))


def cash(amt):
    return Scale(Const(amt), One("USD"))


msft = stock("MSFT")

exchangeRates = dict(
    USD=binomial_price_tree(365, 1, 0),
    GBP=binomial_price_tree(365, 1.55, .0467),
    EUR=binomial_price_tree(365, 1.21, .0515)
)
lookup = dict(
    MSFT=binomial_price_tree(365, 45.48, .220),
    ORCL=binomial_price_tree(365, 42.63, .1048),
    EBAY=binomial_price_tree(365, 53.01, .205)
)

marketData = Environment(
    binomial_price_tree(365, .15, .05),
    exchangeRates,
    lookup)

portfolio = [
    One("USD"),
    stock("MSFT"),
    buy(stock("MSFT"), 45),
    sell(stock("MSFT"), 45),
    option(buy(stock("MSFT"), 45)),
    option(sell(stock("MSFT"), 45)),
    One("EUR"),
    Scale(Const(-1), One("USD")),
    Give(One("USD")),
    Give(Give(One("USD"))),
    And(One("USD"), One("USD")),
    And(One("USD"), Give(One("USD"))),
    When(datetime.date.today(), One("USD")),
    When(datetime.date.today() + datetime.timedelta(days=5), One("USD")),
    When(datetime.date.today() + datetime.timedelta(days=5), cash(49)),
    european_call_option(datetime.date.today() + datetime.timedelta(days=5), stock("MSFT"), 45),
    european_put_option(datetime.date.today() + datetime.timedelta(days=5), stock("MSFT"), 45),
    american_call_option(datetime.date.today() + datetime.timedelta(days=5), stock("MSFT"), 45),
    american_put_option(datetime.date.today() + datetime.timedelta(days=5), stock("MSFT"), 45)
]


def main():
    for contract in portfolio:
        print '==========='
        propt = contract_to_PROpt(contract)
        rp = binomial_valuation(propt, marketData)
        print 'Contract:' + str(contract)
        print 'Present val:' + str(rp.start_val())
        print 'Random Process \n' + str(rp)


if __name__ == '__main__':
    main()
