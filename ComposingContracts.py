from abc import ABCMeta


class Zero:
    def __init__(self):
        pass

    def __str__(self):
        return "Zero()"


class One:
    def __init__(self, currency):
        self.currency = currency

    def __str__(self):
        return "One(" + str(self.currency) + ")"


class Give:
    def __init__(self, contract):
        self.contract = contract

    def __str__(self):
        return "Give(" + str(self.contract) + ")"


class And:
    def __init__(self, contract1, contract2):
        self.contract1 = contract1
        self.contract2 = contract2

    def __str__(self):
        return "And(" + str(self.contract1) + "," + str(self.contract2) + ")"


class Or:
    def __init__(self, contract1, contract2):
        self.contract1 = contract1
        self.contract2 = contract2

    def __str__(self):
        return "Or(" + str(self.contract1) + "," + str(self.contract2) + ")"


class Cond:
    def __init__(self, cond, contract1, contract2):
        self.cond = cond
        self.contract1 = contract1
        self.contract2 = contract2

    def __str__(self):
        return "Cond(" + str(self.cond) + "," + str(self.contract1) + "," + str(self.contract2) + ")"


class Scale:
    def __init__(self, scale, contract):
        self.scale = scale
        self.contract = contract

    def __str__(self):
        return "Scale(" + str(self.scale) + "," + str(self.contract) + ")"


class When:
    def __init__(self, date, contract):
        self.date = date
        self.contract = contract

    def __str__(self):
        return "When(" + str(self.date) + "," + str(self.contract) + ")"


class Anytime:
    def __init__(self, date, contract):
        self.date = date
        self.contract = contract

    def __str__(self):
        return "Anytime(" + str(self.date) + "," + str(self.contract) + ")"


class Obs:
    __metaclass__ = ABCMeta

    def plus(self, that):
        return Lift2(lambda tup: tup[0] + tup[1], self, that)

    def minus(self, that):
        return Lift2(lambda tup: tup[0] - tup[1], self, that)

    def times(self, that):
        return Lift2(lambda tup: tup[0] * tup[1], self, that)

    def eq(self, that):
        return Lift2(lambda tup: tup[0] == tup[1], self, that)

    def neq(self, that):
        return Lift2(lambda tup: tup[0] != tup[1], self, that)

    def gt(self, that):
        return Lift2(lambda tup: tup[0] > tup[1], self, that)

    def lt(self, that):
        return Lift2(lambda tup: tup[0] < tup[1], self, that)

    def gte(self, that):
        return Lift2(lambda tup: tup[0] >= tup[1], self, that)

    def lte(self, that):
        return Lift2(lambda tup: tup[0] <= tup[1], self, that)


class Const(Obs):
    def __init__(self, k):
        self.k = k

    def __str__(self):
        return "Const(" + str(self.k) + ")"


class Lift(Obs):
    def __init__(self, lifted, o):
        self.lifted = lifted
        self.o = o

    def __str__(self):
        return "Lift(" + str(self.lifted) + "," + str(self.o) + ")"


class Lift2(Obs):
    def __init__(self, lifted, o1, o2):
        self.lifted = lifted
        self.o1 = o1
        self.o2 = o2

    def __str__(self):
        return "Lift2(" + str(self.lifted) + "," + str(self.o1) + "," + str(self.o2) + ")"


class DateObs(Obs):
    def __init__(self):
        pass

    def __str__(self):
        return "DateObs()"


class Lookup(Obs):
    def __init__(self, lookup):
        self.lookup = lookup

    def __str__(self):
        return "Lookup(" + str(self.lookup) + ")"
